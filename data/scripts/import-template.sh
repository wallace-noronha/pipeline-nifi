#!/bin/bash
cd /opt/nifi/templates/

probe=`curl -o /dev/null -s -w "%{http_code}\n" http://nifi:8080/nifi-api/flow/about`

echo $probe
sleep 2

while [ $probe != 200 ];
    do
        echo "NOT READY!!!"
        sleep 2
        probe=`curl -o /dev/null -s -w "%{http_code}\n" http://nifi:8080/nifi-api/flow/about`
        echo $probe
done

echo "READY!!!!!!!!!"

ARQUIVOS=`ls .`

for ARQUIVO in $ARQUIVOS
    do  
        curl -X POST http://nifi:8080/nifi-api/process-groups/root/templates/upload -H "Content-Type: multipart/form-data" -F "template=@/opt/nifi/templates/$ARQUIVO" -i
done
 
