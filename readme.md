# PIPELINE INGESTÃO

##### Pipeline de ingestão de dados utilizando o Jenkins e NIFI como motor.


## Como funciona?

Este projeto disponibiliza um pipeline jenkins para deploy de um process-group com base em um template disponível na prateleira de templates do Nifi.
O pipeline verifica os templates disponíveis no Nifi, fornece os inputs necessários para que o usuário preencha somente as informações pertinentes ao template, abstraindo assim toda a interação com o Nifi para o pipeline, facilitando o processo de deploy e centralizando as ingestões.

## Como utilizar
Para utilização do pipeline, foi criado um job no Jenkins com o nome de PIPELINE-NIFI, as confirações foram persistidas no diretório /data.
Desta forma, basta executar o pipeline para iniciar o processo!

## Importando templates para o Nifi
Os templates do Nifi são importados de forma automatica ao iniciar o Nifi, para isto, basta que ele esteja na pasta /data/templates.
Ao iniciar os templates são importados de forma automatica para o Nifi.
Porém, caso queira utilizar um novo template através da esteira, basta importá-lo para o Nifi.
Lembre-se de adicionar o template a pasta /data/templates para que ele seja persistido.
