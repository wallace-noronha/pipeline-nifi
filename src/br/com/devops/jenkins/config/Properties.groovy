package br.com.devops.jenkins.config

class Properties {

    List executedSteps = new ArrayList<String>()

    def scm = [
            "gitRepository" :"",
            "gitBranch"     :"",
            "gitCommit"     :"",
            "gitCommitHash" :""
    ]

    def nifi = [
            "url"               : "",
            "processGroupName"  : "",
            "processGroupId"    : "",
            "template": [
                    "name"      : "",
                    "id"        : "",
                    "variables" : [:]
            ]

    ]

}
