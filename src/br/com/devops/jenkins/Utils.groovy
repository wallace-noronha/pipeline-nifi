package br.com.devops.jenkins

import groovy.json.JsonSlurper
import groovy.json.JsonSlurperClassic
import org.jenkinsci.plugins.workflow.actions.LabelAction
import org.jenkinsci.plugins.workflow.cps.CpsThread

void printMessage(String label) {
    try {
        echo("")
    } finally {
        CpsThread.current().head.get().addAction(new LabelAction("${label} "))
    }
}

void printMessage(String msg, String label) {
    try {
        echo(msg)
    } finally {
        CpsThread.current().head.get().addAction(new LabelAction("${label} "))
    }
}

def jsonParse(def json) {
    new JsonSlurper().parseText(json)
}




