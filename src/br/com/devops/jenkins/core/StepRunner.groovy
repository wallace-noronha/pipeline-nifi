package br.com.devops.jenkins.core

interface StepRunner {

    void run()

}