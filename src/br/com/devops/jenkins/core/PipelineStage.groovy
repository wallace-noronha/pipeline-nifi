package br.com.devops.jenkins.core

class PipelineStage {

    String name
    List<PipelineStep> pipeSteps

    PipelineStage(String name, List<PipelineStep> pipeSteps) {
        this.name = name
        this.pipeSteps = pipeSteps
    }
}
