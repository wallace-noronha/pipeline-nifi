package br.com.devops.jenkins.core

import br.com.devops.jenkins.config.Properties

class PipelineStep {
    def jenkins

    String name
    Properties properties = new Properties()
    List<StepRunner> pipeStepRunners

    PipelineStep(jenkins, Properties props, String name, List<StepRunner> pipeStepRunners) {
        this.jenkins = jenkins
        this.properties = props
        this.name = name
        this.pipeStepRunners = pipeStepRunners
    }

    void executeRunners() {
        this.pipeStepRunners.each {
            it.run()
        }

        properties.executedSteps.add(this.name)
        properties.executedSteps = properties.executedSteps.unique()
    }
}
