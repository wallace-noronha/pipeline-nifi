package br.com.devops.jenkins.core

import br.com.devops.jenkins.Utils
import br.com.devops.jenkins.config.Properties

abstract class StepRunnerBase implements StepRunner, Serializable {

    Properties props
    def jenkins
    String echo
    Utils utils = new Utils()

    StepRunnerBase(Properties props, jenkins){
        this.props = props
        this.jenkins = jenkins
    }

    @Override
    void run() {

    }
}
