package br.com.devops.jenkins.builder

import br.com.devops.jenkins.config.Properties
import br.com.devops.jenkins.core.PipelineStage
import br.com.devops.jenkins.core.PipelineStep
import br.com.devops.jenkins.runners.nifi.*

class PipelineBuilderNifi {

    PipelineBuilderNifi() {
    }

    static PipelineStage[] buildPipeline(Properties props, jenkins) {

        jenkins.echo "JENKINS VARIABLES: ${jenkins.env.getEnvironment()}"

        List<PipelineStage> pipeStages = []

        PipelineStep pgName = new PipelineStep(jenkins, props, "Choice PG", [
                new ChoicePG(props, jenkins)
        ])

        PipelineStep getTemplate = new PipelineStep(jenkins, props, "Template", [
                new GetTemplates(props, jenkins)
        ])

        PipelineStep downloadTemplate = new PipelineStep(jenkins, props, "Variables", [
                new UpdateVariables(props, jenkins)
        ])

        PipelineStep deployTemplate = new PipelineStep(jenkins, props, "Deploy", [
                new DeployTemplate(props, jenkins)
        ])

        PipelineStep startFlow = new PipelineStep(jenkins, props, "Starting", [
                new StartProcessGroup(props, jenkins)
        ])

        pipeStages.add(new PipelineStage("Process-group", [pgName]))
        pipeStages.add(new PipelineStage("Select template", [getTemplate]))
        pipeStages.add(new PipelineStage("Update", [downloadTemplate]))
        pipeStages.add(new PipelineStage("Nifi", [deployTemplate]))
        pipeStages.add(new PipelineStage("Flow", [startFlow]))

        return pipeStages
    }
}
