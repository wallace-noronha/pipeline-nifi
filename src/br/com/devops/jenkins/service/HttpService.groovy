package br.com.devops.jenkins.service

import com.cloudbees.groovy.cps.NonCPS
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.apache.http.HttpEntity
import org.apache.http.HttpHost
import org.apache.http.auth.AuthScope
import org.apache.http.auth.UsernamePasswordCredentials
import org.apache.http.client.AuthCache
import org.apache.http.client.CredentialsProvider
import org.apache.http.client.methods.*
import org.apache.http.client.protocol.HttpClientContext
import org.apache.http.entity.StringEntity
import org.apache.http.impl.auth.BasicScheme
import org.apache.http.impl.client.BasicAuthCache
import org.apache.http.impl.client.BasicCredentialsProvider
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.util.EntityUtils

import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

/**
 *
 * This service wraps the original apache http client
 *
 * Response pattern:
 *
 * Map<String,String> Response
 *  - String statusCode (Response Status Code)
 *  - String headers (Response Readers)
 *  - String content (Response content)
 *  - String requestLog (Request Log)
 *  - String responseLog (Response Log)
 *
 *  Usage:
 *
 *  HttpService hs = new HttpService("http://my-endpoint.com")
 *  Map<String,String> result = hs.get("/api")
 *
 *  println result["content"]
 *
 *
 *  IMPORTANT
 *
 *  When "response.execute" return a null object (Undentified error)
 *  the statusCode will return 000
 *  and the responseLog return "Undentified error on perform a request to $url"
 *
 *
 *
 * @author Wallace Vidal
 * @since 2020-08-01
 *
 */
class HttpService {

    private String url
    private String username
    private String password
    Boolean redirectHandling
    private String proxy = null


    /**
     *
     * Contructor that require a base URL
     *
     * @param String url
     */
    HttpService(String url) {
        this(url, null, null)
    }

    /**
     *
     * Constructor that require a base URL, username and password for simple authentication
     *
     * @param String url
     */
    HttpService(String url, String username, String password) {
        this.url = url
        this.username = username
        this.password = password
        this.redirectHandling = true
    }

    /**
     *
     * Constructor that require a base URL, username and password for simple authentication
     *
     * @param String url
     */
    HttpService(String url, String username, String password, String proxy) {
        this.url = url
        this.username = username
        this.password = password
        this.redirectHandling = true
        this.proxy = proxy
    }

    /**
     * Simple post on base URL without a body and specific path
     *
     * @return Map<String,String>
     */
    @NonCPS
    Map<String, String> post() {
        return this.post(null, null, null)
    }

    /**
     *
     * Simple post on base URL and Path without a body and headers
     *
     * @param String path
     * @return Map<String,String>
     */
    @NonCPS
    Map<String, String> post(String path) {
        return this.post(path, null, "")
    }


    /**
     *
     * Simple post on base URL and Path with headers
     *
     * @param String path
     * @param <String ,String>   header
     *
     * @return Map<String,String>
     */
    @NonCPS
    Map<String, String> post(String path, Map<String, String> header) {
        return this.post(path, header, "")
    }


    /**
     *
     * Full post on base URL and Path with a body and header
     * Important! The Body content-type body must be provided on header
     *
     * @param path
     * @param header
     * @param body
     * @return
     */
    @NonCPS
    Map<String, String> post(String path, Map<String, String> header, String body) {

        path = path != null ? path : ""
        URL targetUrl = new URL(this.url + path)
        CloseableHttpResponse response
        CloseableHttpClient httpClient

        Map<String, Object> result = [:]

        HttpHost httpHost = new HttpHost(targetUrl.getHost(), targetUrl.getPort(), targetUrl.getProtocol())

        try {

            HttpClientContext context = getContext(httpHost)
            httpClient = getHttpClient(httpHost)
            HttpPost request = new HttpPost(targetUrl.toURI())

            String bodyEntity = null

            // Include custom headers
            header.each { k, v -> request.setHeader(k, v) }

            //Include body
            if (body != null) {
                request.setEntity(new StringEntity(body))
                bodyEntity = EntityUtils.toString(request.getEntity())
            }

            def outputRequest = this.getRequestLog(request, bodyEntity)

            response = httpClient.execute(request, context)

            String outputMessage = response.getEntity() ? EntityUtils.toString(response.getEntity()) : "Empty Message"
            String statusCode = response.getStatusLine().getStatusCode()
            String responseHeaders = response.getHeaders()

            def outputResponse = this.getResponseLog(response, outputMessage,responseHeaders)

            result = output(result, statusCode, responseHeaders, outputMessage, outputRequest, outputResponse)

        } catch (Exception ex) {
            throw new RuntimeException(ex)
        } finally {
            if (response != null)
                response.close()
            if (httpClient != null) {
                httpClient.close()
            }
        }

        return result
    }


    /**
     *
     * Full put on base URL and Path with a body and header
     * Important! The Body content-type body must be provided on header
     *
     * @param path
     * @param header
     * @param body
     * @return
     */
    @NonCPS
    Map<String, String> put(String path, Map<String, String> header, String body) {
        path = path != null ? path : ""
        URL targetUrl = new URL(this.url + path)
        CloseableHttpResponse response
        CloseableHttpClient httpClient

        Map<String, Object> result = [:]

        HttpHost httpHost = new HttpHost(targetUrl.getHost(), targetUrl.getPort(), targetUrl.getProtocol())

        try {
            HttpClientContext context = getContext(httpHost)
            httpClient = getHttpClient(httpHost)
            HttpPut request = new HttpPut(targetUrl.toURI())
            String bodyEntity = null

            // Include custom headers
            header.each { k, v -> request.setHeader(k, v) }

            //Include body
            if (body != null) {
                request.setEntity(new StringEntity(body))
                bodyEntity = EntityUtils.toString(request.getEntity())
            }

            def outputRequest = this.getRequestLog(request, bodyEntity)

            response = httpClient.execute(request, context)

            String outputMessage = response.getEntity() ? EntityUtils.toString(response.getEntity()) : "Empty Message"
            String statusCode = response.getStatusLine().getStatusCode()

            Map<String, String> responseHeaders = [:]
            response.getAllHeaders().each {
                responseHeaders.put(it.getName(), it.getValue())
            }

            def outputResponse = this.getResponseLog(response, outputMessage,responseHeaders)

            result = output(result, statusCode, responseHeaders, outputMessage, outputRequest, outputResponse)

        } catch (Exception ex) {
            throw new RuntimeException(ex)
        } finally {
            if (response != null)
                response.close()
            if (httpClient != null) {
                httpClient.close()
            }
        }
        return result
    }


    /**
     *
     * Simple get on base URL without path or headers
     *
     * @return
     */
    @NonCPS
    Map<String, String> get() {
        return this.get("", null)
    }

    /**
     *
     * Simple get on base URL and Path without headers
     *
     * @return
     */
    @NonCPS
    Map<String, String> get(String path) {
        return this.get(path, null)
    }



    /**
     *
     * Get on base URL an path with a header's map
     *
     * @param path
     * @param header
     * @return
     */
    @NonCPS
    Map<String, String> get(String path, Map<String, String> header) {
        URL targetUrl = new URL(this.url + path)
        CloseableHttpResponse response
        CloseableHttpClient httpclient
        Map<String, Object> result = [:]

        HttpHost httpHost = new HttpHost(targetUrl.getHost(), targetUrl.getPort(), targetUrl.getProtocol())

        try {
            HttpClientContext context = getContext(httpHost)
            httpclient = getHttpClient(httpHost)
            HttpGet request = new HttpGet(targetUrl.toURI())
            header.each { k, v -> request.setHeader(k, v) }

            def outputRequest = this.getRequestLog(request, "")

            response = httpclient.execute(httpHost, request, context)

            String outputMessage = response.getEntity() ? EntityUtils.toString(response.getEntity()) : "Empty Message"
            String statusCode = response.getStatusLine().getStatusCode()
            String responseHeaders = response.getHeaders()

            def outputResponse = this.getResponseLog(response, outputMessage,responseHeaders)

            result = output(result, statusCode, responseHeaders, outputMessage, outputRequest, outputResponse)

        } catch (Exception ex) {
            throw new RuntimeException(ex)
        } finally {
            if (response != null) {
                response.close()
            }
            if (httpclient != null) {
                httpclient.close()
            }
        }

        return result
    }

    @NonCPS
    Map<String, String> delete() {
        return this.delete("", null)
    }

    @NonCPS
    Map<String, String> delete(String path) {
        return this.delete(path, null)
    }



    @NonCPS
    Map<String, String> delete(String path, Map<String, String> header) {
        URL targetUrl = new URL(this.url + path)
        CloseableHttpResponse response
        CloseableHttpClient httpclient
        Map<String, Object> result = [:]

        HttpHost httpHost = new HttpHost(targetUrl.getHost(), targetUrl.getPort(), targetUrl.getProtocol())

        try {
            HttpClientContext context = getContext(httpHost)
            httpclient = getHttpClient(httpHost)
            HttpDelete request = new HttpDelete(targetUrl.toURI())
            header.each { k, v -> request.setHeader(k, v) }

            def outputRequest = this.getRequestLog(request, "")

            response = httpclient.execute(httpHost, request, context)

            String outputMessage = response.getEntity() ? EntityUtils.toString(response.getEntity()) : "Empty Message"
            String statusCode = response.getStatusLine().getStatusCode()
            String responseHeaders = response.getHeaders()

            def outputResponse = this.getResponseLog(response, outputMessage,responseHeaders)

            result = output(result, statusCode, responseHeaders, outputMessage, outputRequest, outputResponse)

        } catch (Exception ex) {
            throw new RuntimeException(ex)
        } finally {
            if (response != null) {
                response.close()
            }
            if (httpclient != null) {
                httpclient.close()
            }
        }

        return result
    }

    /**
     *
     * Full post on base URL and Path with a body and header
     *
     * @param path
     * @param header
     * @param body
     * @return
     */
    @NonCPS
    Map<String, String> postMultipart(String path, Map<String, String> header, HttpEntity body) {
        path = path != null ? path : ""
        URL targetUrl = new URL(this.url + path)
        CloseableHttpResponse response
        CloseableHttpClient httpClient

        Map<String, Object> result = [:]

        HttpHost httpHost = new HttpHost(targetUrl.getHost(), targetUrl.getPort(), targetUrl.getProtocol())

        try {
            HttpClientContext context = getContext(httpHost)
            httpClient = getHttpClient(httpHost)
            HttpPost request = new HttpPost(targetUrl.toURI())
            String bodyEntity = null

            header.each { k, v -> request.setHeader(k, v) }

            if (body != null) {
                request.setEntity(body)
                bodyEntity = request.getEntity().getContentType()
            }

            def outputRequest = this.getRequestLog(request, bodyEntity)

            response = httpClient.execute(request, context)


            String outputMessage = response.getEntity() ? EntityUtils.toString(response.getEntity()) : "Empty Message"
            String statusCode = response.getStatusLine()
            String responseHeaders = response.getHeaders()

            def outputResponse = this.getResponseLog(response, outputMessage,responseHeaders)

            result = output(result, statusCode, responseHeaders, outputMessage, outputRequest, outputResponse)


        } catch (Exception ex) {
            throw new RuntimeException(ex);
        } finally {
            if (response != null)
                response.close()
            if (httpClient != null) {
                httpClient.close()
            }
        }
        return result
    }

    /**
     *
     * Resolving HttpContext based on a possible BaseAtuh
     *
     * @param httpHost
     * @return HttpClientContext
     */
    @NonCPS
    private HttpClientContext getContext(HttpHost httpHost) {

        HttpClientContext context = HttpClientContext.create()

        if (null != username && !username.isEmpty()) {

            // Create AuthCache instance
            AuthCache authCache = new BasicAuthCache()

            // Generate BASIC scheme object and add it to the local
            // auth cache
            BasicScheme basicAuth = new BasicScheme()
            authCache.put(httpHost, basicAuth)

            // Add AuthCache to the execution context
            context.setAuthCache(authCache)
        }
        return context
    }

    /**
     *
     * Resolving HttpContext based on a possible BaseAtuh
     * By default ignore ssl
     *
     * @param httpHost
     * @return CloseableHttpClient
     */
    @NonCPS
    private CloseableHttpClient getHttpClient(HttpHost httpHost) {

        def nullTrustManager = [
                "checkClientTrusted": { chain, authType -> null },
                "checkServerTrusted": { chain, authType -> null },
                "getAcceptedIssuers": { null }
        ]
        SSLContext sc = SSLContext.getInstance("SSL")
        sc.init(null, [nullTrustManager as X509TrustManager] as TrustManager[], null)

        HttpClientBuilder httpBuilder = HttpClientBuilder.create()
        httpBuilder.setSslcontext(sc)

        if (null != username && !username.isEmpty()) {

            CredentialsProvider credsProvider = new BasicCredentialsProvider()
            credsProvider.setCredentials(
                    new AuthScope(httpHost.getHostName(), httpHost.getPort()),
                    new UsernamePasswordCredentials(username, password))

            httpBuilder.setDefaultCredentialsProvider(credsProvider)
        }

        if (this.proxy != null) {
            URL proxy = new URL(proxy)
            httpBuilder.setProxy(new HttpHost(proxy.getHost(),proxy.getPort()))
        }

        if (!this.redirectHandling) {
            httpBuilder.disableRedirectHandling()
        }

        return (CloseableHttpClient) httpBuilder.build()
    }

    /**
     *
     * Test whether a String can be parsed in a JSON object
     *
     * @param String test
     * @return boolean
     */
    @NonCPS
    private static boolean isJSON(String test) {
        try {
            new JsonSlurper().parseText(test)
        } catch (ignored) {
            return false
        }
        return true
    }

    @NonCPS
    def getRequestLog(def request, def bodyEntity){

        StringBuilder outputRequest = new StringBuilder()
        outputRequest.append "\n"
        outputRequest.append "\n########################################################################################################################"
        outputRequest.append "\nREQUEST                                                                                                                 "
        outputRequest.append "\n########################################################################################################################"
        outputRequest.append "\n" + request
        outputRequest.append "\n------------------------------------------------------------------------------------------------------------------------"
        outputRequest.append "\n Headers: " + request.getHeaders()
        outputRequest.append "\n------------------------------------------------------------------------------------------------------------------------"
        outputRequest.append "\n" + request.getProperties()
        outputRequest.append "\n------------------------------------------------------------------------------------------------------------------------"
        outputRequest.append "\n Body: " + (isJSON(bodyEntity) ? JsonOutput.prettyPrint(bodyEntity) : bodyEntity)
        outputRequest.append "\n########################################################################################################################"
        outputRequest.append "\n"

        return outputRequest
    }

    @NonCPS
    def getResponseLog(def response, def outputMessage, def responseHeaders){

        StringBuilder outputResponse = new StringBuilder()
        outputResponse.append "\n"
        outputResponse.append "\n########################################################################################################################"
        outputResponse.append "\nRESPONSE                                                                                                                "
        outputResponse.append "\n########################################################################################################################"
        outputResponse.append "\n" + response
        outputResponse.append "\n------------------------------------------------------------------------------------------------------------------------"
        outputResponse.append "\n Headers: " + responseHeaders
        outputResponse.append "\n------------------------------------------------------------------------------------------------------------------------"
        outputResponse.append "\n Body: " + (isJSON(outputMessage) ? JsonOutput.prettyPrint(outputMessage) : outputMessage)
        outputResponse.append "\n########################################################################################################################"
        outputResponse.append "\n"

        return outputResponse
    }

    @NonCPS
    def output(Map<String, Object> result, statusCode, responseHeaders, outputMessage, outputRequest, outputResponse){

        result.put("code", statusCode)
        result.put("headers", responseHeaders)
        result.put("content", outputMessage)
        result.put("request", outputRequest.toString())
        result.put("response", outputResponse.toString())

        return result
    }

}
