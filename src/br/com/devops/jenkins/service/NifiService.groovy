package br.com.devops.jenkins.service

import br.com.devops.jenkins.Utils
import com.fasterxml.jackson.databind.ObjectMapper
import groovy.json.internal.LazyMap
import groovy.xml.XmlUtil
import org.apache.http.entity.ContentType
import org.apache.http.entity.mime.MultipartEntityBuilder
import org.json.JSONObject

class NifiService{

    protected HttpService httpService
    protected Utils utils
    static final String boundary = "---------------"+UUID.randomUUID().toString();
    def jenkins

    NifiService(String url, jenkins){
        this.httpService = new HttpService(url)
        this.utils = new Utils()
        this.jenkins = jenkins
    }

    def getProcessGroups(def name){
        def response = httpService.get("/process-groups/root/process-groups")

        if (this.handleResponse(response)){
            def json = utils.jsonParse(response.content)
            for (def process: json.processGroups){
                if (process.component.name.toString().equalsIgnoreCase(name)){
                    utils.printMessage("ERRO, JÁ EXISTE UM PROCESS-GROUP COM O NOME ${name}, SELECIONE OUTRO NOME!")
                    return false
                }
            }
            return true
        } else {
            jenkins.error "😥 ERRO AO BUSCAR OS PROCESS-GROUPS!"
        }

    }

    def getTemplates(){
        def response = httpService.get("/flow/templates")

        if (this.handleResponse(response)) {
            def json = utils.jsonParse(response.content)
            def templates = [:]
            for (def template : json.templates) {
                templates.put(template.template.name, template.template.id)
            }
            return templates
        }else {
            jenkins.error "😥 ERRO NA BUSCA DOS TEMPLATES!"
        }
    }

    def getTemplate(def templateId){
        def response = httpService.get("/templates/${templateId}/download")
        if (handleResponse(response)) {
            return response.content
        } else {
            jenkins.error "😥 ERRO AO BUSCAR O TEMPLATE COM O ID ${templateId}"
        }
    }

    def getTemplateVariables(def templateId){

        def xml = new XmlSlurper().parseText(getTemplate(templateId))

        LinkedList<String> variables = new ArrayList<>()

        xml.snippet.processGroups.variables.entry.each { variable ->
            variables.add(variable.key.text())
        }

        return variables
    }

    def newTemplate(def templateId, def variables, def prosessGroupName){
        def template = new XmlParser().parseText(getTemplate(templateId))

        template.snippet.processGroups.name[0].value = prosessGroupName
        template.name[0].value = prosessGroupName

        for(def var: variables.keySet()){
            template.snippet.processGroups.variables.entry.each { variable ->
                if (variable.key.text() == var){
                    variable.getAt("value")[0].value = variables.get(var)
                }
            }
        }

        def xml = XmlUtil.serialize(template).toString()

        return xml.toString()
    }

    def uploadTemplate(InputStream template){

        def path = "/process-groups/root/templates/upload"

        Map<String,String> header = [:]
        header.put("Content-type","multipart/form-data; boundary=${boundary}")

        MultipartEntityBuilder builder = MultipartEntityBuilder.create()
                .addBinaryBody("template", template , ContentType.MULTIPART_FORM_DATA,"Template.xml")

        builder.setBoundary(boundary)

        def result = httpService.postMultipart(path, header, builder.build())
        if (this.handleResponse(result)) {
            utils.printMessage(result.toString(), "POST MULTIPART")

            def xmlSlurper = new XmlParser().parseText(result.content)
            def templateId = xmlSlurper.template.id.text()

            return templateId
        } else {
            jenkins.error "😥 ERRO AO EFETUAR O UPLOAD DO TEMPLATE!"
        }
    }

    def instantiateProcessGroup(String templateId){
        def path = "/process-groups/root/template-instance"
        def body = new JSONObject()
        Map<String, Double> position = getRandomPosition()
        body.put("originX", position.x)
        body.put("originY", position.y)
        body.put("templateId","${templateId}")
        Map<String,String> header = [:]

        header.put("Content-Type", "application/json")

        def result = httpService.post(path,header,body.toString())
        if (this.handleResponse(result)){
            def json = utils.jsonParse(result.content)
            return json
        } else {
            jenkins.error "😥 ERRO AO INSTANCIAR O PROCESS-GROUP COM O ID ${templateId}"
        }
    }

    private Map getRandomPosition() {
        Random r = new Random()
        def X_MAX = 2000
        def Y_MAX = 2000

        double x = 0 + (X_MAX + 1) * r.nextDouble()
        double y = 0 + (Y_MAX + 1) * r.nextDouble()

        return ['x': x, 'y': y]
    }

    def deleteTemplateById(String templateId){
        def path = "/templates/${templateId}"
        def retorno = httpService.delete(path)

        if (this.handleResponse(retorno)){
            return retorno.content
        } else {
            jenkins.error "😥 ERRO AO DELETAR O TEMPLATE TEMPORÁRIO COM O ID ${templateId}!"
        }

    }

    def startControllerServices(String processGroupId){
        def lista =  getAllControllerServices(processGroupId)

        Map<String,String> header = [:]
        header.put("Content-Type", "application/json")

        for (ArrayList controller: lista) {
            def path = "/controller-services/${controller.id}/run-status"
            def json = [:]

            json.put("revision", controller.revision)
            json.put("disconnectedNodeAcknowledged", true)
            json.put("state", "ENABLED")
            JSONObject body = new JSONObject(json)
            def result = httpService.put(path, header, body.toString())

            def retorno = utils.jsonParse(result.content)

            if (this.handleResponse(result)) {
                utils.printMessage("CONTROLLER SERVICE: ${retorno.component.name} STARTADO COM SUCESSO!")
            } else {
                jenkins.error "😥 ERRO NO START DO CONTROLLER SERVICE ${retorno.component.name} DO PROCESS-GROUP ID ${processGroupId}!"
            }
        }
    }

    def getAllControllerServices(String processGroupId){
        def path = "/flow/process-groups/${processGroupId}/controller-services?includeDescendantGroups=true&includeAncestorGroups=false"
        def retorno = httpService.get(path)
        utils.printMessage(retorno.toString(), "RETORNO")

        if (this.handleResponse(retorno)){
            def json = utils.jsonParse(retorno.content)

            return json.controllerServices
        } else {
            jenkins.error "😥 ERRO NA BUSCA DOS CONTROLLERS SERVICES DO PROCESS-GROUP ID ${processGroupId}"
        }
    }

    def startProcessors(String processGroupId){
        def path = "/flow/process-groups/${processGroupId}"

        def body = new JSONObject()
        body.put("id",processGroupId)
        body.put("disconnectedNodeAcknowledged", true)
        body.put("state","RUNNING")

        Map<String,String> header = [:]
        header.put("Content-Type", "application/json")

        def response = httpService.put(path,header,body.toString())

        if (this.handleResponse(response)){
            return response.content
        } else {
            jenkins.error "😥 ERRO AO STARTAR O PROCESS-GROUP COM O ID ${processGroupId}"
        }
    }

    def handleResponse(def request){
        if (Integer.valueOf(request.code.find("\\d{3}")) >= 400) {
            utils.printMessage("REQUEST ERROR!")
            utils.printMessage("STATUS CODE: ${request.code}")
            utils.printMessage(request.request.toString(),"REQUEST:")
            utils.printMessage(request.headers.toString(),"RESPONSE HEADER:")
            utils.printMessage(request.content.toString(), "RESPONSE BODY")
            utils.printMessage(request.response.toString(),"RESPONSE:")
            return false
        } else {
            return true
        }
    }

}
