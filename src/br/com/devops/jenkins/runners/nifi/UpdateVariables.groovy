package br.com.devops.jenkins.runners.nifi

import br.com.devops.jenkins.config.Properties
import br.com.devops.jenkins.core.StepRunnerBase
import br.com.devops.jenkins.service.NifiService

class UpdateVariables extends StepRunnerBase{

    UpdateVariables(Properties props, Object jenkins) {
        super(props, jenkins)
    }

    @Override
    void run(){
        utils.printMessage(props.nifi.url, "NIFI URL")

        NifiService nifiService = new NifiService(props.nifi.url, jenkins)

        LinkedList<String> variables = nifiService.getTemplateVariables(props.nifi.template.id)

        utils.printMessage("INFORME AS VARIÁVEIS!")

        def input = jenkins.input message: '🧐 Informe o valor das variaveis do template:', parameters: variables.collect {  k ->
                    [$class: 'StringParameterDefinition', description: k.toString(), name: k.toString()]
        }

        utils.printMessage(input.toString(), "INPUTS")

        props.nifi.template.variables.putAll(input)
    }
}
