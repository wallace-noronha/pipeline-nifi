package br.com.devops.jenkins.runners.nifi

import br.com.devops.jenkins.config.Properties
import br.com.devops.jenkins.core.StepRunnerBase
import br.com.devops.jenkins.service.NifiService

class GetTemplates extends StepRunnerBase{

    GetTemplates(Properties props, Object jenkins) {
        super(props, jenkins)
    }

    @Override
    void run(){
        utils.printMessage(props.nifi.url, "NIFI URL")

        NifiService nifiService = new NifiService(props.nifi.url, jenkins)

        def templates = nifiService.getTemplates()

        if (templates.size() == 0){
            jenkins.error "😥 NÃO EXISTE NENHUM TEMPLATE NO NIFI!"
        }

        def input = jenkins.input message: '🤔 Selecione o template que deseja utilizar!',
            parameters: [
                    jenkins.choice ( choices: templates.collect {  k, v -> k }, description: "Template", name: "template" ),
            ]

        props.nifi.template.name = input
        props.nifi.template.id = templates.get(input)

        utils.printMessage(props.nifi.template.name ,"😉 TEMPLATE SELECIONADO")
    }
}
