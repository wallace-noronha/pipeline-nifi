package br.com.devops.jenkins.runners.nifi

import br.com.devops.jenkins.config.Properties
import br.com.devops.jenkins.core.StepRunnerBase
import br.com.devops.jenkins.service.NifiService

class StartProcessGroup extends StepRunnerBase{

    StartProcessGroup(Properties props, Object jenkins) {
        super(props, jenkins)
    }

    @Override
    void run(){
        utils.printMessage(props.nifi.url, "NIFI URL")

        NifiService nifiService = new NifiService(props.nifi.url, jenkins)

        utils.printMessage("😌 STARTANDO CONTROLLERS SERVICES!")
        nifiService.startControllerServices(props.nifi.processGroupId)

        jenkins.sleep time: 2, unit: 'SECONDS'

        utils.printMessage("😁 STARTANDO PROCESS-GROUP!")
        def response = nifiService.startProcessors(props.nifi.processGroupId)

        utils.printMessage(response.toString(),"👏 🎊 🎉 BUILD FINALIZADO, CONFIRA O RESULTADO NO NIFI!")

    }
}
