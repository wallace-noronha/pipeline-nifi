package br.com.devops.jenkins.runners.nifi

import br.com.devops.jenkins.config.Properties
import br.com.devops.jenkins.core.StepRunnerBase
import br.com.devops.jenkins.service.NifiService

class DeployTemplate extends StepRunnerBase{

    DeployTemplate(Properties props, Object jenkins) {
        super(props, jenkins)
    }

    @Override
    void run(){
        utils.printMessage(props.nifi.url, "NIFI URL")

        NifiService nifiService = new NifiService(props.nifi.url, jenkins)

        utils.printMessage("GENERATE A NEW TEMPLATE")
        def template = nifiService.newTemplate(props.nifi.template.id, props.nifi.template.variables, props.nifi.processGroupName)

        utils.printMessage("NEW TEMPLATE GENERATED!")
        InputStream inputStream = new ByteArrayInputStream(template.getBytes("UTF-8"))

        String templateId = nifiService.uploadTemplate(inputStream)

        utils.printMessage("NEW TEMPLATE ID: ${templateId}","INSTANTIATING YOUR PROCESS-GROUP")

        def deploy = nifiService.instantiateProcessGroup(templateId)

        props.nifi.processGroupId = deploy.flow.processGroups[0].id

        utils.printMessage("ID PROCESS-GROUP DEPLOYED: ${props.nifi.processGroupId}","DEPLOY RESULT OK!")

        nifiService.deleteTemplateById(templateId)

        utils.printMessage("DELETE TEMPORARY TEMPLATE OK!")
    }
}
