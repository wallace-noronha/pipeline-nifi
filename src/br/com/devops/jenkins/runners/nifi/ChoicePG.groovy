package br.com.devops.jenkins.runners.nifi

import br.com.devops.jenkins.config.Properties
import br.com.devops.jenkins.core.StepRunnerBase
import br.com.devops.jenkins.service.NifiService

class ChoicePG extends StepRunnerBase{

    ChoicePG(Properties props, Object jenkins) {
        super(props, jenkins)
    }

    @Override
    void run() {
        utils.printMessage(props.nifi.url, "NIFI URL")

        def retorno = false
        def name = ""

        NifiService nifiService = new NifiService(props.nifi.url, jenkins)

        utils.printMessage("CRIANDO UM NOME PARA SEU PROCESS-GROUP")

        while (!retorno){
            name = answer()
            retorno = nifiService.getProcessGroups(name)
        }

        props.nifi.processGroupName = name

        utils.printMessage(props.nifi.processGroupName, "😁 O NOME DO SEU PROCESS-GROUP SERÁ:")

    }

    def answer(){
        def input = jenkins.input message: 'Qual será o nome do seu process-group?',
                parameters: [
                        [$class: 'StringParameterDefinition', description: 'PROCESS GROUP', name: 'processGroup']
                ]

        return input
    }

}
