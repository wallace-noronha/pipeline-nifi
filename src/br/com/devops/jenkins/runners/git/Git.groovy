package br.com.devops.jenkins.runners.git

import br.com.devops.jenkins.config.Properties
import br.com.devops.jenkins.core.StepRunnerBase

class Git extends StepRunnerBase{

    Git(Properties props, Object jenkins) {
        super(props, jenkins)
    }

    @Override
    void run() {

        utils.printMessage("Branch:${props.scm.gitBranch}", "Iniciando Checkout do Repositorio:${props.scm.gitRepository}")
        jenkins.checkout scm: [$class: 'GitSCM', userRemoteConfigs: [[url: "${props.scm.gitRepository}", credentialsId: 'jenkinsgitlab']], branches: [[name: "${props.scm.gitBranch}"]]], poll: false

        props.scm.gitCommitHash = utils.getCommitHash(jenkins)
        jenkins.echo "Commit Hash ${props.scm.gitCommitHash}"
        jenkins.sh "ls"

    }

}
