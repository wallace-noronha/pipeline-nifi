import br.com.devops.jenkins.builder.PipelineBuilderNifi
import br.com.devops.jenkins.config.Properties
import br.com.devops.jenkins.core.PipelineStage
import br.com.devops.jenkins.core.PipelineStep
import br.com.devops.jenkins.core.StepRunner

def call(body) {

    def pipelineParameters = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = pipelineParameters
    body()

    println "pipelineParams: " + pipelineParameters

    Properties properties = new Properties()

    properties.scm.gitRepository = pipelineParameters.repository
    properties.scm.gitBranch = env.gitlabSourceBranch ? env.gitlabSourceBranch : pipelineParameters.branch


    def pipelineBuilder = new PipelineBuilderNifi()
    PipelineStage[] pipelineStages = pipelineBuilder.buildPipeline(properties, this)

    PipelineStage currentFailedStage

    pipeline {
        agent any
        stages {
            stage('Building pipe') {
                steps {
                    script {
                        def NIFI_URL = sh script: 'echo $NIFI_URL', returnStdout: true
                        properties.nifi.url = NIFI_URL.toString().replaceAll("\\n","").replaceAll("\\r","")

                        for (PipelineStage pipelineStage : pipelineStages) {
                            try {
                                Map mSteps = [:]
                                pipelineStage.pipeSteps.each { pStep ->
                                    mSteps.put(pStep.name, { pStep.executeRunners() })
                                }
                                stage(pipelineStage.name) {
                                    parallel mSteps
                                }
                            } catch (e) {
                                echo "Pipeline Stage exception: ${e}"
                                currentFailedStage = pipelineStage

                                throw e
                            }
                        }
                    }
                }
            }
        }
        post {
            failure {
                script {
                    if (currentFailedStage) {

                        echo "Stage falha: ${currentFailedStage.name}"

                        List<PipelineStep> pipeSteps = currentFailedStage.pipeSteps
                        pipeSteps.each { pipeStep ->
                            List<StepRunner> pipeStepRunners = pipeStep.pipeStepRunners
                            pipeStepRunners.get(0).class
                        }
                    }
                }
            }
        }
    }
}

return this